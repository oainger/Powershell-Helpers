﻿function Get-ScriptRoot {
  [CmdLetBinding()]
  param ()

  try {
    $ScriptRoot = Split-Path -Path $Global:MyInvocation.MyCommand.Path
  }
  catch {
    Write-Host 'Could not detemine Global Script Root' -ForegroundColor Red
    throw $_.Exception.Message
  }

  return $ScriptRoot
}