#requires -Version 2
function Invoke-LegacyWebRequest 
{
  [CmdletBinding()]
  param(
    # URI
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $Uri,
        
    # Method
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [ValidateSet('GET','PUT','POST','DELETE','PATCH')]
    [String]
    $Method = 'GET',
        
    # Content Type
    [Parameter(Mandatory = $false)]
    [String]
    $ContentType,
        
    # Body
    [Parameter(Mandatory = $false)]
    [String]
    $Body,
        
    # Credential
    [Parameter(Mandatory = $false)]
    $Credentials
  )
    
  begin {
  }
    
  process {
    # Setup Web Request
    $Request = [System.Net.WebRequest]::Create($Uri)

    $Request.Method = $Method
    if ($ContentType) 
    {
      $Request.ContentType = $ContentType
    }
    if ($Credentials) 
    {
      $Request.Credentials = $Credentials
    }    
    
    if ($Method -eq 'POST')
    {
      try 
      {
        $RequestStream = $Request.GetRequestStream()
        $StreamWriter = New-Object -TypeName System.IO.StreamWriter -ArgumentList ($RequestStream)
        $StreamWriter.Write($Body)
      }
      catch 
      {
        'ERROR'
        throw $_.Exception.Message
      }
      finally 
      {
        if ($StreamWriter) 
        {
          $StreamWriter.Close()
          $StreamWriter.Dispose()
        }
        if ($RequestStream) 
        {
          $RequestStream.Close()
          $RequestStream.Dispose()
        }
      }
    }    


    try 
    {
      $Response = $Request.GetResponse()
      $ResponseStream = $Response.GetResponseStream()
      $StreamReader = New-Object -TypeName System.IO.StreamReader -ArgumentList ($ResponseStream)
      $ResponseFromServer = $StreamReader.ReadToEnd()
    }
    catch 
    {
      'ERROR'
      throw $_.Exception.Message
    }
    finally 
    {
      if ($StreamReader) 
      {
        $StreamReader.Close()
        $StreamReader.Dispose()
      }
      if ($Response) 
      {
        $Response.Close()
        $Response.Dispose()
      }
    }
  }
        
  end {
    return $ResponseFromServer
  }
}
