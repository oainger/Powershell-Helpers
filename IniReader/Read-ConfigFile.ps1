﻿#requires -Version 2

# function to parse config file in to Hashtable

function Read-ConfigFile 
{
  [OutputType([Hashtable])] 
  [CmdletBinding()]
  param(
    # Config File Path
    [Parameter(Mandatory = $false)]
    [System.IO.FileInfo]
    $ConfigFile
  )
    
  begin {       
    Write-Verbose -Message 'Loading Config'
    $Config = @{}
  }
    
  process {
    if (!(Test-Path -Path $ConfigFile.FullName)) 
    {
      # Error
      throw "Could not find Config at $($ConfigFile.FullName)"
    }
        
    switch -Regex -File $ConfigFile {
      # Skip Comment Block
      '^[;|#]' 
      {
        Write-Verbose -Message 'Skipping Comment'
        continue
      }
      # Load Key/Value pair
      '(.+?)\s*=(.*)' 
      {
        Write-Verbose -Message 'Parsing Key Value pair'
        $Name, $Value = $Matches[1..2]
        $Name = $Name.Trim()
        $Value = $Value.Trim()
        $Config.Add($Name,$Value)
        continue
      }
      # Skip blank line
      '^$' 
      {
        Write-Verbose -Message 'Skipping blank line'
        continue
      }
      # Skip other content 
      '.*' 
      {
        Write-Warning -Message "Cannot parse line '$_'"
        Write-Verbose -Message 'Skipping line'
        continue
      }
    }
  }
    
  end {
    Write-Verbose -Message 'Config Read successfully'
    Return $Config
  }
}

