﻿#requires -Version 2

# function to parse INI file in to Objects
function Read-IniFile 
{
  [OutputType([Hashtable])] 
  [CmdletBinding()]
  param(
    # Config File Path
    [Parameter(Mandatory = $false)]
    [System.IO.FileInfo]
    $IniFile
  )
    
  begin {       
    Write-Verbose -Message 'Loading INI'
    $Ini = New-Object System.Collections.ArrayList
    $Section = $false
    $KeyValue = @{}

  }
    
  process {
    if (!(Test-Path -Path $IniFile.FullName)) 
    {
      # Error
      throw "Could not find Config at $($IniFile.FullName)"
    }
        
    switch -Regex -File $IniFile {
      # Skip Comment Block
      '^[;|#]' 
      {
        Write-Verbose -Message 'Skipping Comment'
        continue
      }
      # Load Section
      '^\[(.*)\]$'
      {
        if ($Section)
        {
          # Write out current section before parsing next
          $Properties = @{
            'Section' = $Section
            'Config' = $KeyValue
          }
          $IniObj = New-Object -TypeName PSObject -Property $Properties
          $IniObj.PSObject.TypeNames.Insert(0,'Ini.Config')
          $null = $Ini.Add($IniObj)
          $KeyValue = @{}
        }
        $Section = $Matches[1]
        Write-Verbose "Found Section: $Section"
        
        continue
      }
      # Load Key/Value pair
      '(.+?)\s*=(.*)' 
      {
        if (!$Section)
        {
          throw 'Invalid INI, no section defined before Key/Values found'
        }
        Write-Verbose -Message 'Parsing Key Value pair'
        $Name, $Value = $Matches[1..2]
        $Name = $Name.Trim()
        $Value = $Value.Trim()
        
        $KeyValue.Add($Name,$Value)
        continue
      }
      # Skip blank line
      '^$' 
      {
        Write-Verbose -Message 'Skipping blank line'
        continue
      }
      # Skip other content 
      '.*' 
      {
        Write-Warning -Message "Cannot parse line '$_'"
        Write-Verbose -Message 'Skipping line'
        continue
      }
    }

    # Write out last section found
    if ($Section -ne $Ini[-1].Section)
    {
      $Properties = @{
        'Section' = $Section
        'Config' = $KeyValue
      }
      $IniObj = New-Object -TypeName PSObject -Property $Properties
      $IniObj.PSObject.TypeNames.Insert(0,'Ini.Config')
      $null = $Ini.Add($IniObj)
    }
  }
    
  end {
    Write-Verbose -Message 'INI Read successfully'
    Return $Ini
  }
}
