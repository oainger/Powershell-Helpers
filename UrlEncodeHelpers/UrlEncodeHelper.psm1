﻿function Get-UrlEncodedString 
{
  [CmdletBinding(DefaultParameterSetName = 'String')]
  param(
    # String to encode
    [Parameter(
        Position = 0,
        ParameterSetName = 'String' 
    )]
    [String]
    $StringToEncode,
        
    # Hashtable to encode
    [Parameter(
        Position = 0,
        ParameterSetName = 'Hashtable'
    )]
    [HashTable]
    $HashtableToEncode
        
  )
    
  begin {
    $StringCollection = @()
    Add-Type -AssemblyName System.Web
  }
    
  process {
    if ($StringToEncode) 
    {
      $EncodedString = [System.Web.HttpUtility]::UrlEncode($StringToEncode)
    }
    if ($HashtableToEncode) 
    {
      foreach ($Item in $HashtableToEncode.GetEnumerator()) 
      {
        $Key = $Item.Key
        $Value = $Item.Value
        $EncodedKey = [System.Web.HttpUtility]::UrlEncode($Key)
        $EncodedValue = [System.Web.HttpUtility]::UrlEncode($Value)
        $String = "$EncodedKey=$EncodedValue"
        $StringCollection += $String
      }
      $EncodedString = $StringCollection -join '&'
    }
        
  }
    
  end {
    return $EncodedString
  }
}

function Get-UrlDecodedString 
{
  [CmdletBinding()]
  param(
    # String to encode
    [Parameter(
        Mandatory = $true,
        Position = 0
    )]
    [ValidateNotNullOrEmpty()]
    [String]
    $StringToDecode   
  )
    
  begin {
    Add-Type -AssemblyName System.Web
  }
    
  process { 
    $DecodedString = [System.Web.HttpUtility]::UrlDecode($StringToDecode)
  }
    
  end {
    return $DecodedString
  }
}